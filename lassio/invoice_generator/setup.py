from setuptools import find_packages, setup

setup(
    name="invoice_generator",
    packages=find_packages(exclude=["invoice_generator_tests"]),
    install_requires=[
        "dagster",
        "pandas",
        "weasyprint"
    ],
    extras_require={"dev": ["dagit", "pytest"]},
)
