from dagster import asset, DynamicOutput, job
import pandas as pd
import os
import random
from datetime import date
import weasyprint

from jinja2 import Environment, FileSystemLoader


pd.options.display.max_rows = 500
pd.options.display.max_columns = 500

@asset
def get_visits():
    return (pd.read_csv('invoice_generator/csv/visits.csv'))


@asset
def filter_approvals(get_visits):
    filtered_df = get_visits.loc[(get_visits['status'] == 'approved')]
    print(filtered_df)
    return filtered_df

@asset
def get_workers():
    return(pd.read_csv('invoice_generator/csv/workers.csv'))


@asset
def group_approvals(filter_approvals,get_workers):
    group_df = pd.merge(filter_approvals,get_workers,left_on='visitor',right_on='workers_name')
    print(group_df)
    return group_df


@asset
def calculate_total(group_approvals):
    group_approvals['Total'] = group_approvals.time * group_approvals.workers_costs_per_hour
    print(group_approvals)
    return group_approvals


@asset
def create_template_list(calculate_total):
    group_df = calculate_total
    new_lst = list()
    for index in group_df.index:
        new_df = {'customers_name': group_df.loc[index,"name"],
                    'date_of_visit': group_df.loc[index,"date"],
                    'number_of_hours': group_df.loc[index,"time"],
                    'charges': group_df.loc[index,"workers_costs_per_hour"],
                    'invoice_number' : random.randint(100000,999999),
                    'sys_date': date.today().strftime("%B %d, %Y"),
                    'worker_name': group_df.loc[index,"visitor"],
                    'worker_address': group_df.loc[index,"workers_address"],
                    'worker_phone' : group_df.loc[index,"workers_phone"],
                    'total_amount': group_df.loc[index,"Total"]
                    }
        new_lst.append(new_df)
    print(new_lst)
    return new_lst

@asset
def process_templates(create_template_list):
    new_lst = create_template_list
    merged_lst = list()
    for i in range(len(new_lst)):
        for j in range(i + 1, len(new_lst)):
            if new_lst[i]['worker_name'] == new_lst[j]['worker_name']:
                key = [k for k in new_lst[i].keys()]
                d_merged = {k: [new_lst[i][k],new_lst[j][k]] for k in key}
                merged_lst.append(d_merged)
            else:
                merged_lst.append(new_lst[i])
    res = []
    [res.append(x) for x in merged_lst if x not in res]        
    return res


@asset
def create_invoice(process_templates):
    ROOT = os.getcwd()
    TEMPLATES_DIR = os.path.join(ROOT,'invoices')
    OUTPUT_DIR = os.path.join(ROOT,'outputs')
    env = Environment(loader=FileSystemLoader(TEMPLATES_DIR))
    template= env.get_template('index.html')

    if len(process_templates) > 1:
        for rows in process_templates:
            OUTPUT_FILE = '''{worker}_invoice.pdf'''.format(worker=rows['worker_name'])
            rendered_template = template.render(rows)
            html = weasyprint.HTML(string=rendered_template)
            invoice = os.path.join(OUTPUT_DIR,OUTPUT_FILE)
            html.write_pdf(invoice)