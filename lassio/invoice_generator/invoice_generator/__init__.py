from dagster import Definitions, load_assets_from_modules, load_assets_from_package_module

from . import assets
from invoice_generator import assets

defs = Definitions(
    assets=load_assets_from_package_module(assets))
#defs = Definitions(assets=load_assets_from_modules([assets]))


