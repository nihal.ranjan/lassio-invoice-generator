To run this code :
Please make sure poetry is installed and configured in your system.
Steps:

poetry install
dagit

Copy and paste the URL in a browser, and you can run the jobs from there. 

All generated invoice will be stored in : invoice_generator/templates/outputs/